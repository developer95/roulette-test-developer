# Roulette API

### Guides

To run the project download the repository and open it using an IDE such as eclipse, visual studio or intellij.
#### Prerequisites
* Install Redis on your machine, the default project uses port 6379 and the url localhost, if you want to change it, set the environment variables REDIS_PORT and REDIS_URL.
* Import in Postman the API collection Roulette.postman collection.json that is in the root of the repository.

### Run Application
* At the root of the repository is the application jar roulette-api-service-1.0.0-SNAPSHOT.jar, which can be executed with the following command from a terminal.
```
$ java -jar roulette-api-service-1.0.0-SNAPSHOT.jar
```
**Note:** If you want to make a change at the level (DEBUG, INFO, ERROR) set the APP_LOGGING_LEVEL environment variable or if you want to change the port set the PORT variable.
```
$ java  -DAPP_LOGGING_LEVEL=DEBUG -jar roulette-api-service-1.0.0-SNAPSHOT.jar
```

**Note:** If you want to make a change at the level (DEBUG, INFO, ERROR) set the APP_LOGGING_LEVEL environment variable or if you want to change the port set the PORT variable.

* Inside the root is the Dockerfile, run it to create a docker container.