package se.daga.roulette.core.repositories;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.daga.roulette.core.domain.BetsRoulette;

public interface RedisRepository {

    Mono<Boolean> save(String key, BetsRoulette rouletteBets);

    Mono<BetsRoulette> findOne(String key);

    Mono<Boolean> delete(String key);

    Flux<String> allKeys();
}
