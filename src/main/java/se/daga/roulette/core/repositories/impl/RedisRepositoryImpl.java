package se.daga.roulette.core.repositories.impl;

import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.daga.roulette.core.domain.BetsRoulette;
import se.daga.roulette.core.repositories.RedisRepository;

@Repository
public class RedisRepositoryImpl implements RedisRepository {
    private final ReactiveRedisOperations<String, BetsRoulette> redisOperations;

    public RedisRepositoryImpl(ReactiveRedisOperations<String, BetsRoulette> redisOperations) {
        this.redisOperations = redisOperations;
    }

    @Override
    public Mono<Boolean> save(String key, BetsRoulette rouletteBets) {
        return redisOperations.opsForValue().set(key, rouletteBets);
    }

    @Override
    public Mono<BetsRoulette> findOne(String key) {
        return redisOperations.opsForValue().get(key);
    }

    @Override
    public Mono<Boolean> delete(String key) {
        return redisOperations.opsForValue().delete(key);
    }

    @Override
    public Flux<String> allKeys() {
        return redisOperations.keys("*");
    }
}
