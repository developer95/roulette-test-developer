package se.daga.roulette.core.dtos;

import java.io.Serializable;

public class GetRouletteResponseDTO implements Serializable {
    private static final long serialVersionUID = 7731365414656259660L;
    private String rouletteId;
    private boolean openRoulette;

    public GetRouletteResponseDTO() {
    }

    public GetRouletteResponseDTO(String rouletteId, boolean openRoulette) {
        this.rouletteId = rouletteId;
        this.openRoulette = openRoulette;
    }

    public String getRouletteId() {
        return rouletteId;
    }

    public void setRouletteId(String rouletteId) {
        this.rouletteId = rouletteId;
    }

    public boolean isOpenRoulette() {
        return openRoulette;
    }

    public void setOpenRoulette(boolean openRoulette) {
        this.openRoulette = openRoulette;
    }
}
