package se.daga.roulette.core.dtos;

import java.io.Serializable;

public class OpenRouletteResponseDTO implements Serializable {
    private static final long serialVersionUID = 7682970411640034195L;
    private boolean openRouletteStatus;
    private String openRouletteMessage;

    public OpenRouletteResponseDTO() {
    }

    public OpenRouletteResponseDTO(boolean openRouletteStatus, String openRouletteMessage) {
        this.openRouletteStatus = openRouletteStatus;
        this.openRouletteMessage = openRouletteMessage;
    }

    public boolean isOpenRouletteStatus() {
        return openRouletteStatus;
    }

    public void setOpenRouletteStatus(boolean openRouletteStatus) {
        this.openRouletteStatus = openRouletteStatus;
    }

    public String getOpenRouletteMessage() {
        return openRouletteMessage;
    }

    public void setOpenRouletteMessage(String openRouletteMessage) {
        this.openRouletteMessage = openRouletteMessage;
    }
}
