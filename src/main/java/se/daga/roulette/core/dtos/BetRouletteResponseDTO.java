package se.daga.roulette.core.dtos;

import java.io.Serializable;

public class BetRouletteResponseDTO implements Serializable {
    private static final long serialVersionUID = 2513774148810152799L;
    private boolean betRouletteStatus;
    private String betRouletteMessage;

    public BetRouletteResponseDTO() {
    }

    public BetRouletteResponseDTO(boolean betRouletteStatus, String betRouletteMessage) {
        this.betRouletteStatus = betRouletteStatus;
        this.betRouletteMessage = betRouletteMessage;
    }

    public boolean isBetRouletteStatus() {
        return betRouletteStatus;
    }

    public void setBetRouletteStatus(boolean betRouletteStatus) {
        this.betRouletteStatus = betRouletteStatus;
    }

    public String getBetRouletteMessage() {
        return betRouletteMessage;
    }

    public void setBetRouletteMessage(String betRouletteMessage) {
        this.betRouletteMessage = betRouletteMessage;
    }
}
