package se.daga.roulette.core.dtos;

import java.io.Serializable;

public class CloseRouletteResponseDTO implements Serializable {
    private static final long serialVersionUID = -1887889552335347422L;
    private Integer winnerNumber;
    private Double winnerValueMoney;

    public Integer getWinnerNumber() {
        return winnerNumber;
    }

    public CloseRouletteResponseDTO(Integer winnerNumber, Double winnerValueMoney) {
        this.winnerNumber = winnerNumber;
        this.winnerValueMoney = winnerValueMoney;
    }

    public void setWinnerNumber(Integer winnerNumber) {
        this.winnerNumber = winnerNumber;
    }

    public Double getWinnerValueMoney() {
        return winnerValueMoney;
    }

    public void setWinnerValueMoney(Double winnerValueMoney) {
        this.winnerValueMoney = winnerValueMoney;
    }
}
