package se.daga.roulette.core.dtos;

import se.daga.roulette.core.domain.Color;

public class BetRouletteDTO {
    private final String userId;
    private final String rouletteId;
    private final Integer money;
    private final Integer betNumber;
    private final Color color;

    public BetRouletteDTO(BetRouletteBuilder builder) {
        this.userId = builder.getUserId();
        this.rouletteId = builder.getRouletteId();
        this.money = builder.getMoney();
        this.betNumber = builder.getBetNumber();
        this.color = builder.getColor();
    }

    public String getUserId() {
        return userId;
    }

    public String getRouletteId() {
        return rouletteId;
    }

    public Integer getMoney() {
        return money;
    }

    public Integer getBetNumber() {
        return betNumber;
    }

    public Color getColor() {
        return color;
    }
}
