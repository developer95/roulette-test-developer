package se.daga.roulette.core.dtos;

import se.daga.roulette.core.domain.Color;

public class BetRouletteBuilder {
    private String userId;
    private String rouletteId;
    private Integer money;
    private Integer betNumber;
    private Color color;

    public String getUserId() {
        return userId;
    }

    public BetRouletteBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getRouletteId() {
        return rouletteId;
    }

    public BetRouletteBuilder setRouletteId(String rouletteId) {
        this.rouletteId = rouletteId;
        return this;
    }

    public Integer getMoney() {
        return money;
    }

    public BetRouletteBuilder setMoney(Integer money) {
        this.money = money;
        return this;
    }

    public Integer getBetNumber() {
        return betNumber;
    }

    public BetRouletteBuilder setBetNumber(Integer betNumber) {
        this.betNumber = betNumber;
        return this;
    }

    public Color getColor() {
        return color;
    }

    public BetRouletteBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public BetRouletteDTO builder() {
        return new BetRouletteDTO(this);
    }
}
