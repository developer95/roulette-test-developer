package se.daga.roulette.core.dtos;

import se.daga.roulette.core.domain.Color;

public class RouletteWinnerDTO {
    private final Integer winnerNumber;
    private final Color winnerColor;


    public RouletteWinnerDTO(Integer winnerNumber, Color winnerColor) {
        this.winnerNumber = winnerNumber;
        this.winnerColor = winnerColor;
    }

    public Integer getWinnerNumber() {
        return winnerNumber;
    }

    public Color getWinnerColor() {
        return winnerColor;
    }
}
