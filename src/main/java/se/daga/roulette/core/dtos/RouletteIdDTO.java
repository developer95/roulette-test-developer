package se.daga.roulette.core.dtos;

import java.io.Serializable;

public class RouletteIdDTO implements Serializable {
    private static final long serialVersionUID = -3829542628464729070L;
    private String rouletteId;

    public RouletteIdDTO() {
    }

    public RouletteIdDTO(String rouletteId) {
        this.rouletteId = rouletteId;
    }

    public String getRouletteId() {
        return rouletteId;
    }

    public void setRouletteId(String rouletteId) {
        this.rouletteId = rouletteId;
    }
}
