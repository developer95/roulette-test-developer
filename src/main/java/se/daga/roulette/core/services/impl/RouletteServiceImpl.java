package se.daga.roulette.core.services.impl;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.Logger;
import reactor.util.Loggers;
import se.daga.roulette.core.domain.Bet;
import se.daga.roulette.core.domain.BetWinnerCalculate;
import se.daga.roulette.core.domain.BetsRoulette;
import se.daga.roulette.core.domain.Roulette;
import se.daga.roulette.core.dtos.*;
import se.daga.roulette.core.repositories.RedisRepository;
import se.daga.roulette.core.services.RouletteService;
import se.daga.roulette.core.util.Util;

import static se.daga.roulette.core.util.Util.MAX_MONEY_BET;

@Service
public class RouletteServiceImpl implements RouletteService {
    private static final Logger LOGGER = Loggers.getLogger(RouletteServiceImpl.class);
    private static final String DENIED_STATUS = "denied";
    private static final String SUCCESSFUL_STATUS = "successful";
    private final RedisRepository redisRepository;

    public RouletteServiceImpl(RedisRepository redisRepository) {
        this.redisRepository = redisRepository;
    }

    @Override
    public Mono<RouletteIdDTO> createRoulette() {
        final var uuid = Util.generateUUID();
        return redisRepository.save(uuid, BetsRoulette.getInstance())
                .thenReturn(new RouletteIdDTO(uuid))
                .doOnNext(rouletteIdResponse -> LOGGER.info("Create roulette, id: {}", rouletteIdResponse.getRouletteId()));
    }

    @Override
    public Mono<OpenRouletteResponseDTO> openRouletteStatus(String rouletteId) {
        return redisRepository.findOne(rouletteId)
                .switchIfEmpty(Mono.empty())
                .filter(rouletteBets -> !rouletteBets.isOpenRoulette())
                .flatMap(r -> {
                            r.setOpenRoulette(true);
                            return redisRepository.save(rouletteId, r);
                        }
                )
                .defaultIfEmpty(Boolean.FALSE)
                .map(s -> {
                    final String status = s ? SUCCESSFUL_STATUS : DENIED_STATUS;
                    return new OpenRouletteResponseDTO(s, status);
                })
                .doOnNext(openRouletteResponse -> LOGGER.info("Roulette id: {}, Open? {}", rouletteId, openRouletteResponse.getOpenRouletteMessage()));
    }

    @Override
    public Mono<BetRouletteResponseDTO> betRoulette(BetRouletteDTO betRouletteDTO) {
        return redisRepository.findOne(betRouletteDTO.getRouletteId())
                .switchIfEmpty(Mono.empty())
                .filter(rouletteBets -> rouletteBets.isOpenRoulette() && validateBetRouletteParams(betRouletteDTO))
                .flatMap(rouletteBets -> {
                    final Bet bet = mapperBet(betRouletteDTO);
                    rouletteBets.getBets().add(bet);
                    return redisRepository.save(betRouletteDTO.getRouletteId(), rouletteBets);
                })
                .defaultIfEmpty(Boolean.FALSE)
                .map(s -> {
                    final String status = s ? SUCCESSFUL_STATUS : DENIED_STATUS;
                    return new BetRouletteResponseDTO(s, status);
                })
                .doOnNext(openRouletteResponse -> LOGGER.info("Roulette id: {}, Bet received? {}",
                        betRouletteDTO.getRouletteId(), openRouletteResponse.getBetRouletteMessage()));
    }

    @Override
    public Mono<CloseRouletteResponseDTO> closeRouletteStatus(String rouletteId) {
        final RouletteWinnerDTO winnerRoulette = Roulette.winnerRoulette();
        return redisRepository.findOne(rouletteId)
                .switchIfEmpty(Mono.empty())
                .filter(rouletteBets -> winnerRoulette != null)
                .filter(BetsRoulette::isOpenRoulette)
                .flatMap(rouletteBets -> {
                    final Double winnerValue = BetWinnerCalculate.winnerValue(rouletteBets, winnerRoulette);
                    rouletteBets.setOpenRoulette(false);
                    assert winnerRoulette != null;
                    return redisRepository.save(rouletteId, rouletteBets)
                            .thenReturn(new CloseRouletteResponseDTO(winnerRoulette.getWinnerNumber(), winnerValue));
                }).doOnNext(closeRouletteResponse -> LOGGER.info("Winner number: {}, Winning bet value: {}",
                        closeRouletteResponse.getWinnerNumber(), closeRouletteResponse.getWinnerValueMoney()));
    }

    public Flux<GetRouletteResponseDTO> allRoulette() {
        return redisRepository.allKeys()
                .flatMap(key -> redisRepository.findOne(key)
                        .map(betsRoulette -> new GetRouletteResponseDTO(key, betsRoulette.isOpenRoulette())))
                .doOnComplete(() -> LOGGER.info("Get all roulette's"));
    }

    private boolean validateBetRouletteParams(BetRouletteDTO betRouletteDTO) {
        return ((betRouletteDTO.getBetNumber() != null) ^ (betRouletteDTO.getColor() != null))
                && (betRouletteDTO.getMoney() <= MAX_MONEY_BET);
    }

    private Bet mapperBet(BetRouletteDTO source) {
        final Bet bet = new Bet();
        bet.setUserId(source.getUserId());
        bet.setBetNumber(source.getBetNumber());
        bet.setColor(source.getColor());
        bet.setBetTypeColor(source.getColor() != null);
        bet.setMoney(source.getMoney());
        return bet;
    }
}
