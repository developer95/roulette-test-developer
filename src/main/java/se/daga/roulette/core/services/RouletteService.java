package se.daga.roulette.core.services;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.daga.roulette.core.dtos.*;

public interface RouletteService {
    Mono<RouletteIdDTO> createRoulette();

    Mono<OpenRouletteResponseDTO> openRouletteStatus(String rouletteId);

    Mono<BetRouletteResponseDTO> betRoulette(BetRouletteDTO betRouletteDTO);

    Mono<CloseRouletteResponseDTO> closeRouletteStatus(String rouletteId);

    Flux<GetRouletteResponseDTO> allRoulette();
}
