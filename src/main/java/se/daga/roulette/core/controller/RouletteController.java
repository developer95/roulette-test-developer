package se.daga.roulette.core.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.daga.roulette.core.domain.Color;
import se.daga.roulette.core.dtos.*;
import se.daga.roulette.core.services.RouletteService;

import java.net.URI;
import java.util.Map;

import static se.daga.roulette.core.util.Util.*;

@RestController
public class RouletteController {
    private final RouletteService rouletteService;

    public RouletteController(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    @GetMapping(value = CREATE_ROULETTE_END_POINT)
    public Mono<ResponseEntity<RouletteIdDTO>> createRoulette() {
        return rouletteService.createRoulette()
                .map(rouletteId -> ResponseEntity
                        .created(URI.create(String.join("/", CREATE_ROULETTE_END_POINT, rouletteId.getRouletteId())))
                        .body(rouletteId));
    }

    @PostMapping(value = OPEN_STATUS_ROULETTE_END_POINT)
    public Mono<ResponseEntity<OpenRouletteResponseDTO>> openRoulette(@RequestBody Mono<RouletteIdDTO> rouletteId) {
        return rouletteId.flatMap(id -> rouletteService.openRouletteStatus(id.getRouletteId()))
                .map(ResponseEntity::ok);
    }

    @GetMapping(value = BET_ROULETTE_END_POINT)
    public Mono<ResponseEntity<BetRouletteResponseDTO>> betRoulette(@RequestHeader final Map<String, String> headers,
                                                                    @RequestParam(required = false) Integer betNumber,
                                                                    @RequestParam(required = false) Color color,
                                                                    @RequestParam Integer money, @RequestParam String rouletteId) {
        final String userId = headers.get(USER_ID_HEADER);
        final BetRouletteDTO betRouletteDTO = new BetRouletteBuilder()
                .setUserId(userId)
                .setRouletteId(rouletteId)
                .setBetNumber(betNumber)
                .setMoney(money)
                .setColor(color)
                .builder();
        return Mono.just(betRouletteDTO)
                .flatMap(rouletteService::betRoulette)
                .map(ResponseEntity::ok);
    }

    @PostMapping(value = CLOSE_ROULETTE_END_POINT)
    public Mono<ResponseEntity<CloseRouletteResponseDTO>> closeRoulette(@RequestBody Mono<RouletteIdDTO> rouletteId) {
        return rouletteId.flatMap(id -> rouletteService.closeRouletteStatus(id.getRouletteId()))
                .map(ResponseEntity::ok);
    }

    @GetMapping(value = GET_ALL_ROULETTE_END_POINT)
    public Flux<GetRouletteResponseDTO> allRoulette() {
        return rouletteService.allRoulette();
    }
}
