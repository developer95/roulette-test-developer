package se.daga.roulette.core.domain;

import se.daga.roulette.core.dtos.RouletteWinnerDTO;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Roulette {
    private static final int MAX = 37;
    private static final int MIN = 0;

    private Roulette() {
        throw new UnsupportedOperationException();
    }

    public static RouletteWinnerDTO winnerRoulette() {
        try {
            final Random random = SecureRandom.getInstanceStrong();
            final int winnerNumber = random.nextInt(MAX - MIN) + MIN;
            final Color winnerColor = (winnerNumber % 2 == 0) ? Color.RED : Color.BLACK;
            return new RouletteWinnerDTO(winnerNumber, winnerColor);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
