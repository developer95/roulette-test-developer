package se.daga.roulette.core.domain;

import se.daga.roulette.core.dtos.RouletteWinnerDTO;

import static se.daga.roulette.core.util.Util.FACTOR_WINNER_COLOR;
import static se.daga.roulette.core.util.Util.FACTOR_WINNER_NUMBER;

public class BetWinnerCalculate {
    private static final boolean WINNER_COLOR = true;
    private static final boolean WINNER_NUMBER = false;
    private static boolean betType;

    private BetWinnerCalculate() {
        throw new UnsupportedOperationException();
    }

    public static Double winnerValue(BetsRoulette rouletteBets, RouletteWinnerDTO rouletteWinnerDTO) {
        return rouletteBets.getBets()
                .stream()
                .map(bet -> {
                    if (bet.isBetTypeColor() && bet.getColor().equals(rouletteWinnerDTO.getWinnerColor())) {
                        betType = WINNER_COLOR;
                        return bet;
                    } else if (!bet.isBetTypeColor() && bet.getBetNumber().equals(rouletteWinnerDTO.getWinnerNumber())) {
                        betType = WINNER_NUMBER;
                        return bet;
                    }
                    return bet;
                })
                .map(Bet::getMoney)
                .mapToDouble(Integer::doubleValue).sum() * winnerValueFactor(betType);
    }

    private static Double winnerValueFactor(boolean betType) {
        return (betType) ? FACTOR_WINNER_COLOR : FACTOR_WINNER_NUMBER;
    }
}
