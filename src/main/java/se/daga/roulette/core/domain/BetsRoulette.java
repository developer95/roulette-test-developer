package se.daga.roulette.core.domain;

import java.util.ArrayList;
import java.util.List;

public final class BetsRoulette {
    private boolean openRoulette;

    private List<Bet> bets;

    public BetsRoulette() {
        this.bets = new ArrayList<>();
    }

    public static BetsRoulette getInstance() {
        return new BetsRoulette();
    }

    public boolean isOpenRoulette() {
        return openRoulette;
    }

    public void setOpenRoulette(boolean openRoulette) {
        this.openRoulette = openRoulette;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

}
