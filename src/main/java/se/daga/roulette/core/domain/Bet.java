package se.daga.roulette.core.domain;

public class Bet {
    private boolean betTypeColor;
    private String userId;
    private Integer money;
    private Integer betNumber;
    private Color color;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getBetNumber() {
        return betNumber;
    }

    public void setBetNumber(Integer betNumber) {
        this.betNumber = betNumber;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isBetTypeColor() {
        return betTypeColor;
    }

    public void setBetTypeColor(boolean betTypeColor) {
        this.betTypeColor = betTypeColor;
    }
}
