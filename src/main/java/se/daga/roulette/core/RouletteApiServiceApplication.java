package se.daga.roulette.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RouletteApiServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(
                RouletteApiServiceApplication.class, args);
    }
}
