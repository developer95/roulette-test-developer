package se.daga.roulette.core.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import se.daga.roulette.core.domain.BetsRoulette;

@Configuration
public class RedisConfiguration {
    @Bean
    public ReactiveRedisOperations<String, BetsRoulette> redisOperations(ReactiveRedisConnectionFactory connectionFactory) {
        Jackson2JsonRedisSerializer<BetsRoulette> serializer = new Jackson2JsonRedisSerializer<>(BetsRoulette.class);
        RedisSerializationContext.RedisSerializationContextBuilder<String, BetsRoulette> builder =
                RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
        RedisSerializationContext<String, BetsRoulette> context = builder.value(serializer).build();

        return new ReactiveRedisTemplate<>(connectionFactory, context);
    }
}
