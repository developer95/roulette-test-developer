package se.daga.roulette.core.util;

import java.util.UUID;

public class Util {
    public static final String CREATE_ROULETTE_END_POINT = "/createRoulette";
    public static final String OPEN_STATUS_ROULETTE_END_POINT = "/openRoulette";
    public static final String BET_ROULETTE_END_POINT = "/betRoulette";
    public static final String CLOSE_ROULETTE_END_POINT = "/closeRoulette";
    public static final String GET_ALL_ROULETTE_END_POINT = "/allRoulette";
    public static final String USER_ID_HEADER = "UserId";
    public static final Integer MAX_MONEY_BET = 10000;
    public static final Double FACTOR_WINNER_NUMBER = 8.0;
    public static final Double FACTOR_WINNER_COLOR = 1.8;

    private Util() {
        throw new UnsupportedOperationException();
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
